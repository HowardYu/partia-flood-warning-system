

from floodsystem.stationdata import build_station_list
from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine #import haversine module 
from floodsystem.geo import stations_by_distance


def run():
    # Build list of stations
    stations = build_station_list()

    x = stations_by_distance(stations,(52.2053, 0.1218))

    # print data of the  the 10 closest and the 10 furthest stations from the Cambridge city centre
    print("The 10 closest stations from the Cambridge city centre : {}" .format(x[:10]))
    print("The 10 furthest stations from the Cambridge city centre : {}" .format(x[-10:]))

if __name__ == "__main__":
    run()