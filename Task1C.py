from floodsystem.stationdata import build_station_list
from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine #import haversine module 
from floodsystem.geo import stations_within_radius 

def run():
    
    # Build list of stations
    stations = build_station_list()
    print('stations within 10 km of the Cambridge city centre:{}'.format(stations_within_radius(stations,(52.2053, 0.1218),10)))  

if __name__ == "__main__":
    run()