from floodsystem.stationdata import build_station_list
from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine #import haversine module 
from floodsystem.geo import  rivers_with_station
from floodsystem.geo import stations_by_river

def run():

    # Build list of stations
    stations = build_station_list()
    print("The number of rivers have at least one monitoring station: "+ str(len(rivers_with_station(stations))))
    x = sorted_by_key(rivers_with_station(stations),0)
    print(x[:10])


    Y = stations_by_river(stations)
    Demo_stations =('River Aire','River Cam','River Thames')
    for name in Demo_stations:
        print('stations located on {} : {}' .format(name,sorted(Y[name])))

if __name__ == "__main__":
    run()