from floodsystem.stationdata import build_station_list
from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine #import haversine module 
from floodsystem.geo import  rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def run():
    stations = build_station_list()
    rivers = rivers_by_station_number(stations, 9)

    return print(rivers)

if __name__ == "__main__":
    run()