from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list,update_water_levels

   
def run():
 
    # Build list of stations
    stations = build_station_list()
    # Update water levels
    update_water_levels(stations)
    # Print the stations at which the current relative level is over 0.8
    print(stations_level_over_threshold(stations,0.8))
    
    
if __name__ == "__main__":
    run()