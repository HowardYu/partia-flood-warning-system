from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold,stations_highest_rel_level
from floodsystem.stationdata import build_station_list,update_water_levels

def run():
 
    # Build list of stations
    stations = build_station_list()
    # Update water levels
    update_water_levels(stations)
    # Print 10 stations (objects) at which the water level, relative to the typical range, is highest. 
    print(stations_highest_rel_level(stations,10))
    
if __name__ == "__main__":
    run()