import datetime
import matplotlib.pyplot as plt

from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold,stations_highest_rel_level
from floodsystem.stationdata import build_station_list,update_water_levels
from floodsystem.flood import stations_level_over_threshold,stations_highest_rel_level
from floodsystem.stationdata import build_station_list,update_water_levels
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels


def run():
        # Build list of stations
        stations = build_station_list()
        update_water_levels(stations)
        # Print 10 stations (objects) at which the water level, relative to the typical range, is highest. 
        top5 = stations_highest_rel_level(stations, N = 5)
        
        
        stations_top5 = []
        for entry in top5:
                for station in stations:
                        if entry[0] == station.name :
                                stations_top5.append(station)
        print (stations_top5)
        for station in stations_top5:
                dt = 10
                dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
                #plot the graph for the station
                plot_water_levels(station, dates, levels)
                
if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
