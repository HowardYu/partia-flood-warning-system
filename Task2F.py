import datetime
import matplotlib.pyplot as plt

from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold,stations_highest_rel_level
from floodsystem.stationdata import build_station_list,update_water_levels
from floodsystem.flood import stations_level_over_threshold,stations_highest_rel_level
from floodsystem.stationdata import build_station_list,update_water_levels
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit

def run():
        stations = build_station_list()
        update_water_levels(stations)
        top5 = stations_highest_rel_level(stations, N=5)

        stations_top5 = []
        for entry in top5:
                for station in stations:
                        if entry[0] == station.name :
                                stations_top5.append(station)

        for station in stations_top5:
                print (station.name)
                dt = 2
                dates, levels = fetch_measure_levels(station.measure_id, dt = datetime.timedelta(days=dt))
    
                plot_water_level_with_fit(station, dates, levels, 4)


if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()