import datetime
import matplotlib.pyplot as plt

from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold,stations_highest_rel_level
from floodsystem.stationdata import build_station_list,update_water_levels
from floodsystem.flood import stations_level_over_threshold,stations_highest_rel_level
from floodsystem.stationdata import build_station_list,update_water_levels
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit

def run():
    #Build list of stations most at risk
    stations = build_station_list()
    update_water_levels(stations)
    top10 = stations_highest_rel_level(stations, N=10)

    #Build list of towns the stations are in
    list_of_towns_high = []
    list_of_towns_moderate = []
    list_of_towns_low = []

    for entry in top10:
        for station in stations:
            if station.name == entry[0] and entry[1]>= 2.0:
                list_of_towns_high.append(station.town)#

            elif station.name == entry[0] and entry[1]>=1.5 and entry[1]<2.0:
                list_of_towns_moderate.append(station.town)#

            elif station.name == entry[0] and entry[1]<1.5:
                list_of_towns_low.append(station.town)

    print ("High:" , list_of_towns_high)
    print ("Moderate:" , list_of_towns_moderate)
    print ("Low:" , list_of_towns_low)

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()   
            