"""This module provides a model for a monitoring station, and tools
for water level data

"""
from .station import MonitoringStation,inconsistent_typical_range_stations
from .utils import sorted_by_key

'''
Return a list of stations with relative water level above the tolerant value
'''
def stations_level_over_threshold(stations, tol):
    list_of_stations = [] # Empty list to fill
    for station in stations:
        if MonitoringStation.relative_water_level(station) is not None and MonitoringStation.relative_water_level(station) > tol:
        
            n = station.name
            rwl = MonitoringStation.relative_water_level(station)
            r = (n,rwl)
            list_of_stations.append(r)
    result = sorted_by_key(list_of_stations,1,True)
    return result

'''
Return a list of N stations with a descending order of relative water level 
'''
def stations_highest_rel_level(stations, N):
    list_of_stations = [] # Empty list to fill
    for station in stations:
        if MonitoringStation.relative_water_level(station) is not None:
        
            n = station.name
            rwl = MonitoringStation.relative_water_level(station)
            r = (n,rwl)
            list_of_stations.append(r)
    result = sorted_by_key(list_of_stations,1,True)
    result = result[0:N]
    return result