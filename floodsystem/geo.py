# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
from haversine import haversine #import haversine module 

def stations_by_distance(stations,p):
    result = []
    for i in range(len(stations)): #iterate through the list of stations
        C = stations[i].coord # extract all the coordinates
        r = (stations[i].name,stations[i].town,haversine(p,C)) # put the name of station and its coord into a tuple
        result.append(r)
    result = sorted_by_key(result,2)
    return result

def stations_within_radius(stations, centre, r):
    result = []
    for i in range(len(stations)): #iterate through the list of stations
        C = stations[i].coord # extract all the coordinates 
        n = stations[i].name # extract all the names
        if haversine(centre,C) <= r:
            result.append(n)
    result = sorted_by_key(result,0)
    return result
    

def rivers_with_station(stations):
    result = set()
    for i in range(len(stations)): #iterate through the list of stations
        R = stations[i].river # extract all the rivers
        result.add(R)
    return result
    
def stations_by_river(stations):
    result = {}
    for i in range(len(stations)): #iterate through the list of stations
        R = stations[i].river # extract all the rivers
        if R not in result: 
            result[R] = [str(stations[i].name)] # if the river is not in the dict, create a new key
        else:
            result[R].append(str(stations[i].name)) # if the river is already in the dict, add a new value to it
    return result


def rivers_by_station_number(stations, N):
    """Returns list of tuples of river names and corresponding number of stations"""
    riv_sta = [] # create empty list to fill

    result = stations_by_river( stations ) # call function to build dict of rivers and stations, assign dictionary to variable

    for key, value in result.items(): #iterate through list
        k = (str(key), sum(1 for v in value if v)) # build tuples with keys in dictionary and number of values.
        riv_sta.append(k) # add each tuple to a list
    
    riv_sta = sorted_by_key(riv_sta,1, reverse = True) #sort tubles by number
    riv_sta = riv_sta[:N] # show N rivers in decending order of stations

    return riv_sta


        

