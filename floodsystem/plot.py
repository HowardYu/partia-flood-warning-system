import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from datetime import datetime, timedelta



def plot_water_levels(station, dates, levels):
    """Plots levels angaist dates for a station or list of stations"""

    #Plot date agaisnt level
    plt.plot(dates, levels)
    plt.plot(dates, [station.typical_range[0]]*len(dates))
    plt.plot(dates, [station.typical_range[1]]*len(dates))

    #Add labels to plot
    plt.xlabel("date")
    plt.ylabel("water level (m)")
    plt.xticks(rotation=45)
    plt.title(str(station.name))

    #Display plot
    plt.show()


def plot_water_level_with_fit(station, dates, levels, p):
    #Assign variables
    x = matplotlib.dates.date2num(dates)
    y = levels

    x1 = np.linspace(x[0], x[-1], 30)
    p_coeff = np.polyfit(x - x[0], y, p)
    poly = np.poly1d(p_coeff)

    #Plot date agaisnt level
    plt.plot(x,y)
    plt.plot(x1, poly(x1 - x1[0]))
    
    #Add labels to plot
    plt.xlabel("date")
    plt.ylabel("water level (m)")
    plt.xticks(rotation=45)
    plt.title(station.name)
    #Display plot
    plt.show()
