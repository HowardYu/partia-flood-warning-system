
from floodsystem.utils import sorted_by_key  # noqa
from haversine import haversine #import haversine module 
from floodsystem.station import MonitoringStation
from floodsystem.geo import stations_within_radius

def build_test_stations():
    st1 = MonitoringStation(
        station_id='st1',
        measure_id="st1",
        label='st1',
        coord=(52.2053, 0.1219),
        typical_range=None,
        river='river1',
        town='city1'
        )
    st2 = MonitoringStation(
        station_id='st2',
        measure_id="st2",
        label='st2',
        coord=(52.2053, 0.1290),
        typical_range=None,
        river='river2',
        town='city2'
        )
    st3 = MonitoringStation(
        station_id='st3',
        measure_id="st3",
        label='st3',
        coord=(52.2053, 0.1220),
        typical_range=None,
        river='river3',
        town='city3'
        )   
    return [st1,st2,st3]
    


def test_stations_within_radius():
    stations = build_test_stations()
    x = stations_within_radius(stations,(52.2053, 0.1218),0.1)
    assert x == ["st1",'st3']