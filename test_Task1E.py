from floodsystem.station import MonitoringStation
from floodsystem.utils import sorted_by_key
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def build_test_stations():
    st1 = MonitoringStation(
        station_id='st1',
        measure_id="st1",
        label='st1',
        coord=(52.2053, 0.1219),
        typical_range=None,
        river='river1',
        town='city1'
        )
    st2 = MonitoringStation(
        station_id='st2',
        measure_id="st2",
        label='st2',
        coord=(52.2053, 0.1290),
        typical_range=None,
        river='river1',
        town='city2'
        )
    st3 = MonitoringStation(
        station_id='st3',
        measure_id="st3",
        label='st3',
        coord=(52.2053, 0.1220),
        typical_range=None,
        river='river1',
        town='city3'
        )
    st4 = MonitoringStation(
        station_id='st4',
        measure_id="st4",
        label='st4',
        coord=(52.2053, 0.1220),
        typical_range=None,
        river='river3',
        town='city4'
        )
    st5 = MonitoringStation(
        station_id='st5',
        measure_id="st5",
        label='st5',
        coord=(52.2053, 0.1220),
        typical_range=None,
        river='river2',
        town='city5'
        )
    st6 = MonitoringStation(
        station_id='st6',
        measure_id="st6",
        label='st6',
        coord=(52.2053, 0.1220),
        typical_range=None,
        river='river3',
        town='city6'
        )    
    return [st1,st2,st3,st4,st5,st6]



def test_rivers_by_station_number():
    stations = build_test_stations()
    result = stations_by_river(stations)
    x = rivers_by_station_number (stations ,2)

    assert x == [('river1',3),('river3', 2)]



