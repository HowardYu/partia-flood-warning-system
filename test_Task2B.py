
from floodsystem.utils import sorted_by_key  # noqa
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold

def build_test_stations():
    st1 = MonitoringStation(
        station_id='st1',
        measure_id="st1",
        label='st1',
        coord=(52.2053, 0.1219),
        typical_range=(0.068, 0.42),
        river='river1',
        town='city1'
        )
    st2 = MonitoringStation(
        station_id='st2',
        measure_id="st2",
        label='st2',
        coord=(52.2053, 0.1290),
        typical_range=(0.231, 3.95),
        river='river1',
        town='city2'
        )
    st3 = MonitoringStation(
        station_id='st3',
        measure_id="st3",
        label='st3',
        coord=(52.2053, 0.1220),
        typical_range=None,
        river='river1',
        town='city3'
        )
    st4 = MonitoringStation(
        station_id='st4',
        measure_id="st4",
        label='st4',
        coord=(52.2053, 0.1220),
        typical_range=(0.15, 0.895),
        river='river3',
        town='city4'
        )
    st5 = MonitoringStation(
        station_id='st5',
        measure_id="st5",
        label='st5',
        coord=(52.2053, 0.1220),
        typical_range=(1.2,2.5),
        river='river2',
        town='city5'
        )
    st6 = MonitoringStation(
        station_id='st6',
        measure_id="st6",
        label='st6',
        coord=(52.2053, 0.1220),
        typical_range=(2.0,4.0),
        river='river3',
        town='city6'
        )    
    return [st1,st2,st3,st4,st5,st6]



def test_stations_level_over_threshold():
    stations = build_test_stations()
    # update water level
    stations[0].latest_level = (0.35)
    stations[1].latest_level = (4.0)
    stations[2].latest_level = None
    stations[3].latest_level = (0.5)
    stations[4].latest_level = (2.4)
    stations[5].latest_level = (8.0)
    result = stations_level_over_threshold(stations,0.8)
    assert result == [('st6', 3.0), ('st2', 1.013444474321054), ('st5', 0.923076923076923), ('st1', 0.8011363636363636)]