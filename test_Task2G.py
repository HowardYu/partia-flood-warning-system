from floodsystem.utils import sorted_by_key  # noqa
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.flood import stations_highest_rel_level


def build_test_stations():
    st1 = MonitoringStation(
        station_id='st1',
        measure_id="st1",
        label='st1',
        coord=(52.2053, 0.1219),
        typical_range=(0.068, 0.42),
        river='river1',
        town='city1'
        )
    st2 = MonitoringStation(
        station_id='st2',
        measure_id="st2",
        label='st2',
        coord=(52.2053, 0.1290),
        typical_range=(0.231, 3.95),
        river='river1',
        town='city2'
        )
    st3 = MonitoringStation(
        station_id='st3',
        measure_id="st3",
        label='st3',
        coord=(52.2053, 0.1220),
        typical_range=None,
        river='river1',
        town='city3'
        )
    st4 = MonitoringStation(
        station_id='st4',
        measure_id="st4",
        label='st4',
        coord=(52.2053, 0.1220),
        typical_range=(0.15, 0.895),
        river='river3',
        town='city4'
        )
    st5 = MonitoringStation(
        station_id='st5',
        measure_id="st5",
        label='st5',
        coord=(52.2053, 0.1220),
        typical_range=(1.2,2.5),
        river='river2',
        town='city5'
        )
    st6 = MonitoringStation(
        station_id='st6',
        measure_id="st6",
        label='st6',
        coord=(52.2053, 0.1220),
        typical_range=(2.0,4.0),
        river='river3',
        town='city6'
        )    
    return [st1,st2,st3,st4,st5,st6]

    


def test_list_of_towns():
    stations = build_test_stations()
    
    # update water level
    stations[0].latest_level = (0.35)
    stations[1].latest_level = (4.0)
    stations[2].latest_level = None
    stations[3].latest_level = (0.5)
    stations[4].latest_level = (2.4)
    stations[5].latest_level = (8.0)
    top5 = stations_highest_rel_level(stations, N=5)

    #Build list of towns the stations are in
    list_of_towns_high = []
    list_of_towns_moderate = []
    list_of_towns_low = []

    for entry in top5:
        for station in stations:
            if station.name == entry[0] and entry[1]>= 2.0:
                list_of_towns_high.append(station.town)#

            elif station.name == entry[0] and entry[1]>=1.5 and entry[1]<2.0:
                list_of_towns_moderate.append(station.town)#

            elif station.name == entry[0] and entry[1]<1.5:
                list_of_towns_low.append(station.town)

    assert list_of_towns_high ==['city6']
    assert list_of_towns_moderate == []
    assert list_of_towns_low == ['city2', 'city5', 'city1', 'city4']